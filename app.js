var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');


var Device = require('./Models/Device.model');
var DeviceDAO = require('./DAOs/DeviceDAO');
var Controller = require('./Controller');
var Process = require('./Process');

var port = 8080;

var db = 'mongodb://localhost/db_homecontroller';

var app = express();

mongoose.connect(db);

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}));

/*
    REST requests;
_____
    POST    /device/     (Body)     JSON of Device (Name, PIN);
    PUT     /device/<id>  (Body)    UPdates the Device by id
    GET     /device/<id>            JSON of Device (lightState, Name, ID, Pin, Processes[])
    GET     /device/                JSON returns all Devices
    POST    /device/<id>/<state>   Turns light on or off ; state ("on", "off")
_____
    POST    /process/<deviceID>   (Body) Creats a new process for a device (time, state, period)
    PUT     /process/<processID>    Updates the Process
    


*/


app.listen(port, function() {
  console.log('app listening on port ' + port);
});
